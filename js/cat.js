window.addEventListener(
    "load",
    function () {
        // Needed to allow to play music in bg
        $(".d").click();

        $(".cat").on("click", function (event) {
            play("sitting-cat", event.currentTarget);
        });

        $(".kitty").on("click", function (event) {
            play("window-cat", event.currentTarget);
        });

        $(".popup-cat").on("click", function (event) {
            play("popup-cat", event.currentTarget);
        });
    },
    false
);

var audio;
var currentElementName;
var audioState = "paused";
var musicAnimation;
const msMusicNoteIteration = 800;

function play(cat, element) {
    clearInterval(musicAnimation);
    var elementName = element.getAttribute("data-name");

    if (elementName === currentElementName) {
        if (audioState === "paused") {
            audio.play();
            audio.volume = 0.5;
            audioState = "play";
            musicAnimation = setInterval(function () {
                musicNotes(element);
            }, 800);
            // End animation on song end
        } else {
            audioState = "paused";
            audio.pause();
        }
    } else {
        if (audio !== undefined) {
            audio.pause();
            audio = null;
        }

        switch (elementName) {
            case "Tangerine":
                audio = new Audio("assets/audio/bongo-cat-enemy.mp3");
                // https://www.youtube.com/watch?v=Qs1fwWzPWcE
                break;
            case "Steve":
                audio = new Audio(
                    "assets/audio/bongo-cat-somewhere-only-we-know.mp3"
                );
                // https://www.youtube.com/watch?v=3eEPB1-bXrE
                break;
            case "Oreo":
                audio = new Audio("assets/audio/bongo-cat-see-you-again.mp3");
                // https://www.youtube.com/watch?v=5jyep4LZVuY
                break;
            case "Kimchi":
                audio = new Audio("assets/audio/bongo-cat-flowers.mp3");
                // https://www.youtube.com/watch?v=A0Pntqjzd3k
                break;
            case "Boo":
                audio = new Audio("assets/audio/bongo-cat-blinding-lights.mp3");
                // https://www.youtube.com/watch?v=gH919am-83M
                break;
            default: // Sushi
                audio = new Audio("assets/audio/bongo-cat-wake-me-up.mp3");
                // https://www.youtube.com/watch?v=re_gjYl5_mg
                break;
        }

        // End animation on song end
        audio.addEventListener("ended", function () {
            clearInterval(musicAnimation);
        });

        currentElementName = elementName;
        audioState = "play";
        audio.play();
        audio.volume = 0.5;

        musicAnimation = setInterval(function () {
            musicNotes(element);
        }, 800);
    }
}

function musicNotes(element) {
    const musicNotesArr = [
        // Double music note
        "<div class='parent'><div class='music-note-level-1'><div class='music-note music-note-1'></div>  <div class='music-note music-note-2'></div></div></div>",

        // Single music note 1
        "<div class='parent'><div class='music-note-level-2'><div class='music-note music-note-3'></div></div></div>",

        // Single music note 2
        "<div class='parent'><div class='music-note-level-5'><div class='music-note music-note-4'></div></div></div>",
    ];

    var musicNote = musicNotesArr[getRandomInt(3)];
    var domNodes = $($.parseHTML(musicNote));

    $(element).append(domNodes);
    $(".parent").bind(
        "animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd",
        function (e) {
            $(this).remove();
        }
    );
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}
